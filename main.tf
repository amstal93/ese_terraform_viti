# provider configuration
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}


provider "aws" {
  region  = var.region
}

# s3 backend
terraform {
  backend "s3" {
    bucket = "tf-state-bucket-viti"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

# data sources
data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"]
}

data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.eseviti-wordpressdb.endpoint
  }
}

# outputs
output "instance_public_ip" {
    value = aws_instance.eseviti-wordpressec2.public_ip
}

# variables
variable "region" {
    description = "aws region"
    default = "us-east-1"
}

variable "vpc_id" {
    description = "id of vpc to launch ec2 instance"
    default = "vpc-0d5f4780fcbf6d0a6"
}

variable "ec2_subnet_id" {
    description = "id of subnet to launch ec2 instance"
    default = "subnet-09cc0b65a0639bceb"
}

variable "db_subnet_id" {
    description = "id of subnet to launch ec2 instance"
    default = ["subnet-0335d37569de93d4c","subnet-09cc0b65a0639bceb"]

}

variable "key_name" {
    description = "name of private key to log into the ec2 instance"
    default = "icmpviti"
}

variable "ec2_type" {
    description = "instance type of the ec2 instance"
    default = "t2.micro"
}

variable "db_type" {
    description = "instance type of the rds instance"
    default = "db.t3.micro"
}


variable "database_name" {
    description = "name of the rds database"
    default = "wp_db"
}


variable "database_user" {
    description = "username for rds instance"
    default = "wp_user"
}


variable "database_password" {
    description = "password for rds instance"
    default = "H3!r2g0nd0r"
}

variable "instance_sec_group_name" {
    description = "Name for instance sg"
    default = "instance_sg"
}

variable "db_sec_group_name" {
    description = "Name for db sg"
    default = "db_sg"
}

#resources
resource "aws_instance" "eseviti-wordpressec2" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.ec2_type
  subnet_id = var.ec2_subnet_id
  key_name = var.key_name
  user_data       = data.template_file.user_data.rendered
  vpc_security_group_ids = [aws_security_group.wordpressec2_sg.id]
  associate_public_ip_address = true
  tags = {
    Name = "viti-ec2"
  }


}

resource "aws_db_instance" "eseviti-wordpressdb" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = var.db_type
  name                 = var.database_name
  username             = var.database_user
  password             = var.database_password
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.wordpressdb_sg.id]
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group_name.name
  tags = {
    Name = "viti-dbec2"
  }


}
resource "aws_db_subnet_group" "db_subnet_group_name" {
  name       = "main"
  subnet_ids = var.db_subnet_id

  tags = {
    Name = "My DB subnet group"
  }
}


resource "aws_security_group" "wordpressec2_sg" {
  name        = var.instance_sec_group_name
  description = "Allow http inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "http from internet"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ssh from internet"
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "wordpressdb_sg" {
  name        = var.db_sec_group_name
  description = "Allow http inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "http from internet"
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    security_groups  = [aws_security_group.wordpressec2_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
